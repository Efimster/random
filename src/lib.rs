pub mod x982;
pub mod x962;
use std::time::{SystemTime, Instant, UNIX_EPOCH};
use std::mem;


#[allow(dead_code)]
pub fn generate_bytes() -> [u8; 20] {
    let now = Instant::now();
    let epoh = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();

    let mut a: u32 = 0x67452301;
    let mut b: u32 = 0xEFCDAB89;
    let mut c: u32 = 0x98BADCFE;
    let mut d: u32 = 0x10325476;
    let mut e: u32 = 0xC3D2E1F0;

    for num in 0..80 {
        let f: u32;
        let k: u32;

        if num < 20 {
            f = (b & c) | ((!b) & d);
            k = 0x5A827999;
        } else if num < 40 {
            f = b ^ c ^ d;
            k = 0x6ED9EBA1;
        } else if num < 60 {
            f = (b & c) | (b & d) | (c & d);
            k = 0x8F1BBCDC;
        } else {
            f = b ^ c ^ d;
            k = 0xCA62C1D6;
        }
        let temp: u64 = a.rotate_left(5) as u64 + f as u64 + e as u64 + k as u64
            + epoh + now.elapsed().subsec_nanos() as u64;
        e = d;
        d = c;
        c = b.rotate_left(30);
        b = a;
        a = temp as u32;
    }

    unsafe { mem::transmute([a, b, c, d, e]) }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_generate() {
//        println!("{:x}", generate_u32());
//        println!("{:x}", generate_u32());
//        println!("{:x}", generate_u32());
//        println!("{:x}", generate_u32());
//        println!("{:x}", generate_u32());
//        println!("{:x}", generate_u32());
//        println!("{:?}", generate_bytes());
//        println!("{:?}", generate_bytes());
//        println!("{:?}", generate_bytes());
//        println!("{:?}", generate_bytes());
//        println!("{:?}", generate_bytes());
//        println!("{:?}", generate_bytes());

        assert_eq!(1, 1);
    }
}