use bigint::BigInt;
use sha::sha1;
use std::time::Instant;
use std::time::UNIX_EPOCH;
use std::time::SystemTime;
use bigint::BASE_SHIFT;


///*
/// x9.62
pub fn generate(prime: &BigInt, count: usize, bits: usize, user_input: Option<&[u8]>) -> Box<[BigInt]> {
    debug_assert!(bits <= 512);
    debug_assert!(bits >= 160);
    let s = log_2_n_floor(&prime) + 1;
    let mut f = s / 160;
    if s % 160 > 0 {
        f += 1;
    }
    let mut x_key = get_input_entropy();
    let user_input = match user_input {
        None => BigInt::zero(),
        Some(value) => BigInt::from(value),
    };
    let n_1: BigInt = prime - &BigInt::one();
    let mut results: Vec<BigInt> = Vec::with_capacity(count);
    let zero_bytes = create_buffer((512 - bits) >> 3);
    for _ in 0..count {
        let mut k: Vec<u8> = Vec::with_capacity(f * 20);
        for _ in 0..f {
            let x_seed = user_input.clone();
            let mut x_val: Vec<u8> = Vec::with_capacity(512 >> 3);
            x_val.extend_from_slice(&(&x_key + &x_seed).as_reverse_bytes(false));
            x_val.extend_from_slice(&zero_bytes);
            let x = sha1::encode(&x_val);
            x_key = BigInt::from(&((&x_key + &BigInt::one() + BigInt::from(&x as &[u8])).as_bytes(false)[..(bits >> BASE_SHIFT)]));
            k.extend_from_slice(&x);
        }
        let k = BigInt::from(&k as &[u8]).classic_reduction(&n_1) + BigInt::one();
        results.push(k);
    }

    results.into_boxed_slice()
}

fn log_2_n_floor(n: &BigInt) -> usize {
    let mut index = n.len() - 1;
    while index > 0 {
        let leading_zeros = n[index].leading_zeros() as usize;
        if leading_zeros > 0 {
            return (index << 6) + 64 - leading_zeros - 1;
        }
        index -= 1;
    }

    64 - n[index].leading_zeros() as usize - 1
}

fn get_input_entropy() -> BigInt {
    let now = (Instant::now().elapsed().subsec_nanos() as u64)
        | (Instant::now().elapsed().subsec_nanos() as u64) << 32;
    let epoh = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
    let result1 = epoh ^ now;
    let result2 = result1.rotate_right((result1 & 0x1f) as u32);
    let result3 = result2.rotate_left((result2 & 0x1f) as u32);
    let result: &[u64] = &[result1, result2, result3];
    BigInt::from(result)
}

fn create_buffer(length: usize) -> Box<[u8]> {
    let mut buff: Vec<u8> = Vec::with_capacity(length);
    unsafe { buff.set_len(length); }
    let result: Box<[u8]> = buff.into_boxed_slice();
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn test_log_2_n_floor() {
        assert_eq!(log_2_n_floor(&BigInt::one()), 0);
        assert_eq!(log_2_n_floor(&BigInt::from(2u64)), 1);
        assert_eq!(log_2_n_floor(&BigInt::from(3u64)), 1);
        assert_eq!(log_2_n_floor(&BigInt::from(4u64)), 2);

        let x: BigInt = BigInt::from_str("123456789abcdef01").unwrap();
        assert_eq!(log_2_n_floor(&x), 64);
    }

    #[test]
    fn test_generate() {
        const PRIME: u64 = 1999;
        let result = generate(&BigInt::from(PRIME), 3, 192, None);
        assert_eq!(result.len(), 3);
        assert_eq!(result[0] < PRIME, true);
        assert_eq!(result[1] < PRIME, true);
        assert_eq!(result[2] < PRIME, true);
    }
}