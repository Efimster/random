use std::time::{SystemTime, Instant, UNIX_EPOCH};
use std::mem;
use sha::hmac;

const HASH_LENGTH: usize = 32;

///
/// X9.82 Pseudo-Random Number Generation
/// https://tools.ietf.org/html/rfc4086
pub fn generate_bytes() -> [u8; HASH_LENGTH] {
    let now = (Instant::now().elapsed().subsec_nanos() as u64)
        | (Instant::now().elapsed().subsec_nanos() as u64) << 32;
    let epoh = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();

    let input_entropy: [u8; 8] = unsafe { mem::transmute(epoh ^ now) };
    let mut k: [u8; HASH_LENGTH] = [0; HASH_LENGTH];
    let mut v: [u8; HASH_LENGTH] = [1; HASH_LENGTH];

    //Initializing the Generator
    let mut v_skewed: Vec<u8> = Vec::with_capacity(HASH_LENGTH + 1 + 8);
    v_skewed.extend_from_slice(&v);
    v_skewed.push(0x0);
    v_skewed.extend_from_slice(&input_entropy);


    k = hmac::hmac_sha256(&k, v_skewed.as_slice());
    v = hmac::hmac_sha256(&k, &v);
    v_skewed.clear();
    v_skewed.extend_from_slice(&v);
    v_skewed.push(0x1);
    v_skewed.extend_from_slice(&input_entropy);


    k = hmac::hmac_sha256(&k, v_skewed.as_slice());
    v = hmac::hmac_sha256(&k, &v);

    //Generating Random Bits
    v = hmac::hmac_sha256(&k, &v);
    //finishing
    v_skewed.clear();
    v_skewed.extend_from_slice(&v);
    v_skewed.push(0x0);
    k = hmac::hmac_sha256(&k, &v);
    v = hmac::hmac_sha256(&k, &v);

    v
}